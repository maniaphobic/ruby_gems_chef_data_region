require 'spec_helper'

module Test
  FIXTURES = {
    chef: {
      data_region: {
        bags: {
          'hmm' => {
            attribute: 'whoa',
            pattern: 'hmm-%<attribute>s'
          }
        }
      }
    }
  }.freeze
end

describe 'Chef::DataRegion' do
  before do
    class Chef
      class Recipe
        @node = {
          'whoa' => 'hi!'
        }
      end
    end
    @recipe = Chef::Recipe.new('cookbook', 'default', '.')
    Test::FIXTURES[:chef][:data_region][:bags].each do |bag_name, bag_defn|
      Chef::DataRegion.add(bag_name, bag_defn)
    end
  end

  it 'returns the configured data bags' do
    expect(Chef::DataRegion.bags)
      .to eq(Test::FIXTURES[:chef][:data_region][:bags])
  end

  pending 'tests data_bag_item'
end
