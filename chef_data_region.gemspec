$LOAD_PATH.unshift('lib')

require 'chef/data_region'

gems = {
  development: {
    'rspec' => '~> 3.0',
    'rubocop' => '~> 0.54'
  },
  runtime: {
    'chef' => '> 11'
  }
}

Gem::Specification.new do |spec|
  gems[:development].each do |gem_name, gem_version|
    spec.add_development_dependency(gem_name, gem_version)
  end
  gems[:runtime].each do |gem_name, gem_version|
    spec.add_runtime_dependency(gem_name, gem_version)
  end
  spec.authors     = %w[Someone]
  spec.description = File.read('README.md')
  spec.email       = 's@me.one'
  spec.files       = Dir.glob('lib/**/*.rb')
  spec.homepage    = 'https://so.me.one'
  spec.licenses    = %w[0BSD]
  spec.name        = Chef::DataRegion::NAME
  spec.summary     = 'Access region-specific Chef data bags'
  spec.version     = Chef::DataRegion::VERSION
end
