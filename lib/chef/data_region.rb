require 'chef/data_bag_item'
require 'chef/encrypted_data_bag_item'
require 'chef/recipe'

# Chef class
class Chef
  # DataRegion class
  class DataRegion
    NAME    = 'chef_data_region'.freeze
    VERSION = '1.0.6'.freeze

    # This class variable maps data bags to expansion patterns
    @bags = {}

    # Return the configured data bags
    def self.bags
      @bags
    end

    # Add a bag definition to the set of bags
    def self.add(bag_name, bag_hash)
      @bags[bag_name] = bag_hash
    end

    # DataQuery module
    #
    # This module borrows its name from {Chef::DSL::DataQuery}, which
    # defines the `data_bag_item` method. It exists for mixing in to
    # {Chef::Recipe}, as occurs with {Chef::DSL::DataQuery}. This is
    # the means by which `data_bag_item` is available in Chef recipes
    # without qualification.
    module DataQuery
      # Fetch the specified item from the specified bag
      #
      # @param bag [] data bag name
      # @param item [] data bag item name
      # @param secret [] encrypted data bag item secret key
      # @return [Chef::DataBagItem] the fetched data bag item
      def data_bag_item(bag, item, secret = nil)
        loaded_item = Chef::DataBagItem.load(expand_bag_name(bag), item)
        encrypted?(loaded_item) ? decrypt(loaded_item) : loaded_item
      end

      private

      # Decrypt an encrypted item
      #
      # @param item [Chef::DataBagItem] a data bag item
      def decrypt(item)
        secret ||= Chef::EncryptedDataBagItem.load_secret
        Chef::EncryptedDataBagItem.new(item.raw_data, secret)
      end

      # Is the given item encrypted?
      #
      # @param item [Chef::DataBagItem] a data bag item
      def encrypted?(item)
        item.raw_data.map do |_, value|
          value.respond_to?(:key) && value.key?('encrypted_data')
        end.reduce(false, :|)
      end

      # Expand a data bag name if it matches one of the configured patterns.
      #
      # If the name matches, consult the node attribute specified in
      # the configuration to retrieve the region name, then substitute
      # it into the expansion pattern.
      #
      # If the name does not match, return the name verbatim.
      #
      # @param bag_name [String] a data bag name
      # @return [String] the expanded bag name
      def expand_bag_name(bag_name)
        if Chef::DataRegion.bags.keys.include?(bag_name)
          lambda do |definition|
            format(
              definition[:pattern],
              attribute: definition[:attribute].reduce(node) do |hash, index|
                hash.fetch(index)
              end
            )
          end.call(Chef::DataRegion.bags.fetch(bag_name))
        else
          bag_name
        end
      rescue KeyError
        bag_name
      rescue NoMethodError
        raise("Undefined region for data bag '#{bag_name}'")
      end
    end
  end
end

# Mix the module in to {Chef::Recipe}, thereby overriding
# Chef::DSL::DataQuery#data_bag_item
Chef::Recipe.include(Chef::DataRegion::DataQuery)
